#!/bin/bash

# Function to update variables in the YAML file
update_yaml() {
    local variable_name="$1"
    local new_value="$2"
    sed -i "s/^$variable_name:.*/$variable_name: $new_value/" playbook/group_vars/all.yml
}

# Function to clear variables in the YAML file
clear_yaml() {
    local variable_name="$1"
    sed -i "s/^$variable_name:.*/$variable_name:/" playbook/group_vars/all.yml
}

# List of variables to clear
variables_to_clear=(
    "remote_user"
    "remote_password"
    "beats_password"
    "elasticsearch_node_name_master"
    "elasticsearch_node_name_data"
    "ip_elk_node_1"
    "ip_elk_node_2"
    "elasticsearch_ca_password"
    "elasticsearch_cert_password"
    "elasticsearch_http_password"
    "elasticsearch_bootstrap_password"
    "secu_path_elastic_certs"
)

# Clear values for each variable
for variable_name in "${variables_to_clear[@]}"; do
    clear_yaml "$variable_name"
done
