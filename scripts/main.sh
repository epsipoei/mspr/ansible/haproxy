#!/bin/bash

# Function to update variables in the YAML file
update_all-yaml() {
    local variable_name="$1"
    local new_value="$2"
    sed -i "s/^$variable_name:.*/$variable_name: $new_value/" playbook/group_vars/all.yml
}

# Read and update variables
declare -A variables=(
    ["remote_user"]="debian"
    ["remote_password"]="mspr-@srbd"
    ["beats_password"]="mspr-@srbd"
    ["elasticsearch_node_name_master"]="elk-node-1"
    ["elasticsearch_node_name_data"]="elk-node-2"
    ["ip_elk_node_1"]="10.0.0.10"
    ["ip_elk_node_2"]="10.0.0.11"
    ["elasticsearch_ca_password"]="mspr-@srbd"
    ["elasticsearch_cert_password"]="mspr-@srbd"
    ["elasticsearch_http_password"]="mspr-@srbd"
    ["secu_path_elastic_certs"]="\/elastic_certs"
)

# Update YAML for each variable
for variable_name in "${!variables[@]}"; do
    read -p "$variable_name (default: ${variables[$variable_name]}): " input_value
    value="${input_value:-${variables[$variable_name]}}"
    update_all-yaml "$variable_name" "$value"
done

# Set elasticsearch_bootstrap_password same as beats_password
update_all-yaml "elasticsearch_bootstrap_password" "${variables['beats_password']}"
